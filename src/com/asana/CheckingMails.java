package com.asana;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.elasticsearch.common.inject.spi.Message;
import org.elasticsearch.index.store.Store;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CheckingMails {
	 private static String saveDirectory;

	public static JSONObject check(String host, String storeType, String user, String password) {
		JSONObject jsonOutput = new JSONObject();
		try {

			// create properties field
			Properties properties = new Properties();

			properties.put("mail.pop3.host", host);
			properties.put("mail.pop3.port", "110");
			properties.put("mail.pop3.starttls.enable", "true");
			Session emailSession = Session.getDefaultInstance(properties);

			// create the POP3 store object and connect with the pop server
			javax.mail.Store store = emailSession.getStore("pop3s");

			store.connect(host, user, password);

			// create the folder object and open it
			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);

			// retrieve the messages from the folder in an array and print it
			javax.mail.Message[] messages = emailFolder.getMessages();
			System.out.println("messages.length---" + messages.length);
			if (messages.length > 0) {
				for (int i = 0, n = messages.length; i < n; i++) {
					
					
	                
					javax.mail.Message message = messages[i];
					String contentType = message.getContentType();
	                String messageContent = "";
	                
	             // store attachment file name, separated by comma
	                String attachFiles = "";
	 
	                if (contentType.contains("multipart")) {
	                    // content may contain attachments
	                    Multipart multiPart = (Multipart) message.getContent();
	                    int numberOfParts = multiPart.getCount();
	                    for (int partCount = 0; partCount < numberOfParts; partCount++) {
	                        MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
	                        if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
	                            // this part is attachment
	                            String fileName = part.getFileName();
	                            attachFiles += fileName + ", ";
	                            part.saveFile(saveDirectory + File.separator + fileName);
	                        } else {
	                            // this part may be the message content
	                            messageContent = part.getContent().toString();
	                        }
	                    }
	 
	                    if (attachFiles.length() > 1) {
	                        attachFiles = attachFiles.substring(0, attachFiles.length() - 2);
	                    }
	                } else if (contentType.contains("text/plain")
	                        || contentType.contains("text/html")) {
	                    Object content = message.getContent();
	                    if (content != null) {
	                        messageContent = content.toString();
	                    }
	                }
	                
	                
					System.out.println("---------------------------------");
					System.out.println("Email Number " + (i + 1));
					System.out.println("Subject: " + message.getSubject());
					System.out.println("From: " + message.getFrom()[0]);
					System.out.println("Text: " + message.getContent().toString());

					if (message.getSubject().toString().equalsIgnoreCase("resume")
							|| message.getSubject().toString().equalsIgnoreCase("cv")
							|| message.getSubject().toString().equalsIgnoreCase("biodata")) {

						try {
							String url = "https://app.asana.com/api/1.0/tasks";
							@SuppressWarnings("deprecation")
							HttpClient client = new DefaultHttpClient();
							HttpPost post = new HttpPost(url);
							post.setHeader("Authorization", "Bearer 0/94c97976672d47ec8c4198bed85722b1");
							// add required parameters to API
							List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
							urlParameters.add(new BasicNameValuePair("workspace", "697756242661260"));
							urlParameters.add(new BasicNameValuePair("name",
									"tasks for  " + message.getSubject() + "  " + message.getFrom()[0]));
							urlParameters.add(new BasicNameValuePair("notes",
									"define tasks related to person having mail:  " + message.getFrom()[0]));
							urlParameters.add(new BasicNameValuePair("projects", "697949741515335"));

							jsonOutput = getPostDataOutput(url, client, post, urlParameters);

						} catch (Exception e) {
							e.printStackTrace();
							String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
							throw new Exception(errorMessageAndClassWithMethod + e.toString());
						}
						return jsonOutput;
					} else {
						jsonOutput = (JSONObject) jsonOutput.put("result", "no emails to work on");
					}
				}

			}

			// close the store and folder objects
			emailFolder.close(false);
			store.close();

		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonOutput;
	}

	public static void main(String[] args) {

		String host = "pop.secureserver.net";// change accordingly
		String mailStoreType = "pop3";
		String username = "vyankatesh.repal@zconsolutions.com";// change
																// accordingly
		String password = "vyankateshr";// change accordingly
		String saveDirectory = "D:/Attachment";
		 
		CheckingMails receiver = new CheckingMails();
        receiver.setSaveDirectory(saveDirectory);
        
        
		check(host, mailStoreType, username, password);

	}
	
	   public void setSaveDirectory(String dir) {
	        this.saveDirectory = dir;
	    }

	public static String getErrorContainingClassAndMethod() {
		final StackTraceElement e = Thread.currentThread().getStackTrace()[2];
		final String s = e.getClassName();
		String errorInMethod = s.substring(s.lastIndexOf('.') + 1, s.length()) + "." + e.getMethodName();
		return "Error in " + errorInMethod + " : ";
	}

	/**
	 * @param url
	 * @param client
	 * @param post
	 * @param urlParameters
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 * @throws ClientProtocolException
	 * @throws UnsupportedOperationException
	 * @throws ParseException
	 */
	private static JSONObject getPostDataOutput(String url, HttpClient client, HttpPost post,
			List<NameValuePair> urlParameters) throws UnsupportedEncodingException, IOException,
			ClientProtocolException, UnsupportedOperationException, ParseException {
		JSONObject jsonOutput;
		post.setEntity(new UrlEncodedFormEntity(urlParameters));
		HttpResponse response = client.execute(post);
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		System.out.println(result.toString());
		JSONParser parser = new JSONParser();
		jsonOutput = (JSONObject) parser.parse(result.toString());
		return jsonOutput;
	}
}